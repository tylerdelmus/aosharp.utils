﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraceBot9001.IPCMessages
{
    public enum IPCOpcode
    {
        Start = 1050,
        Stop = 1051,
        Errored = 1052,
        ResetDupeBags = 1053,
        DupeItemsReady = 1054
    }
}
