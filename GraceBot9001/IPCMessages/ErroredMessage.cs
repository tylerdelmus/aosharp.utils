﻿using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraceBot9001.IPCMessages
{
    [AoContract((int)IPCOpcode.Errored)]
    public class ErroredMessage : IPCMessage
    {
        public override short Opcode => (int)IPCOpcode.Errored;
    }
}
