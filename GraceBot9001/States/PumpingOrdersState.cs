﻿using AOSharp.Core.GMI;
using AOSharp.Core.Inventory;
using AOSharp.Core.Misc;
using AOSharp.Core.UI;
using Serilog;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GraceBot9001.States
{
    internal static class PumpingOrdersState
    {
        internal static void Entry()
        {
            RequestGMIInventory();
        }

        private static void RequestGMIInventory()
        {
            GMI.GetInventory().ContinueWith(async marketInventory =>
            {
                if (marketInventory.IsFaulted || marketInventory.Result == null)
                {
                    await Task.Delay(1000);
                    RequestGMIInventory();
                    return;
                }

                OnMarketInventoryLoaded(marketInventory.Result);
            });
        }

        private static void OnMarketInventoryLoaded(MarketInventory marketInventory)
        {
            if (marketInventory.Credits < Main.QueuedCash)
            {
                Task.Delay(2000).ContinueWith((e) => RequestGMIInventory());
                return;
            }
            else if (marketInventory.Credits < 100000000)
            {
                Log.Debug($"We only got {marketInventory.Credits}. Time to dupe more..");
                Main.StateMachine.Fire(Trigger.NeedCredits);
                return;
            }
            else if(marketInventory.Credits > Main.GMI_MAX_USABLE_CREDITS)
            {
                Main.FatalError($"Too many credits in GMI. {marketInventory.Credits:n0} (expected {Main.GMI_MAX_USABLE_CREDITS:n0})");
                return;
            }

            GMI.GetMarketOrders().ContinueWith(marketOrders =>
            {
                if (!marketOrders.IsFaulted && marketOrders.Result != null)
                    OnMarketOrdersLoaded(marketInventory, marketOrders.Result);
            });
        }

        private static void OnMarketOrdersLoaded(MarketInventory marketInventory, MyMarketOrders myOrders)
        {
            MyMarketBuyOrder graceOrder = myOrders.BuyOrders.Where(x => x.Name == Main.PUMP_ITEM_NAME && x.Price < Main.DesiredGracePrice).OrderByDescending(x => x.Price).FirstOrDefault();

            if (graceOrder == null)
            {
                Main.FatalError($"No orders to pump.");
                return;
            }

            long desiredIncrease = Math.Min(Main.DesiredGracePrice - graceOrder.Price, (long)(marketInventory.Credits * 0.995f)) / graceOrder.Count;

            Log.Information($"Increasing Grace order {graceOrder.Id} by {desiredIncrease:N0}.");

            graceOrder.ModifyPrice(graceOrder.Price + desiredIncrease).ContinueWith(modifyOrder =>
            {
                if (modifyOrder.Result.Succeeded)
                {
                    Log.Information($"Grace order {graceOrder.Id} successfully increased to {(graceOrder.Price + desiredIncrease):N0}");
                    Main.QueuedCash = 0;
                    Main.StateMachine.Fire(Trigger.NeedCredits);
                }
                else
                {
                    Main.FatalError($"Failed to modify order {graceOrder.Id}. The error is {modifyOrder.Result.Message}");
                }
            });
        }
    }
}
