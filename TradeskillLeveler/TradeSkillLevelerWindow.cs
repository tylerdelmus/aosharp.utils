﻿using AOSharp.Common.GameData;
using AOSharp.Common.GameData.UI;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Misc;
using AOSharp.Core.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;

namespace TradeskillLeveler
{
    internal class TradeSkillLevelerWindow : AOSharpWindow
    {
        internal Action<ButtonState> ActionButtonClicked;

        private ViewCache _viewCache;
        private ItemCache _itemCache;
        private AutoResetInterval _uiUpdateInterval = new AutoResetInterval(100);
        private AutoResetInterval _precheckStatusUpdateInterval = new AutoResetInterval(1000);

        private ButtonState _buttonState = ButtonState.Start;

        internal TradeSkillLevelerWindow(string pluginDir, ItemCache itemCache) : base("Meow", $"{pluginDir}\\Views\\TradeskillLevelerWindow.xml", WindowStyle.Popup, WindowFlags.AutoScale | WindowFlags.NoFade)
        {
            _itemCache = itemCache;
        }

        protected override void OnWindowCreating()
        {
            _viewCache = new ViewCache();

            if (Window.FindView("Root", out _viewCache.Root))
            {
                _viewCache.Root.FindChild("DesiredLevelSliderLabel", out _viewCache.DesiredLevelSliderLabel);
                _viewCache.Root.FindChild("DesiredLevelSlider", out _viewCache.DesiredLevelSlider);
                _viewCache.Root.FindChild("HackerToolStatusLabel", out _viewCache.HackerToolStatusLabel);
                _viewCache.Root.FindChild("ImplantDisassemblyStatusLabel", out _viewCache.ImplantDisassemblyStatusLabel);
                _viewCache.Root.FindChild("Ql1ImplantStatusLabel", out _viewCache.Ql1ImplantStatusLabel);
                _viewCache.Root.FindChild("GraftEquippedStatusLabel", out _viewCache.GraftEquippedStatusLabel);
                _viewCache.Root.FindChild("PoolStatusLabel", out _viewCache.PoolStatusLabel);
                _viewCache.Root.FindChild("ResearchModeToggle", out _viewCache.ResearchModeToggle);
                _viewCache.Root.FindChild("ActionButton", out _viewCache.ActionButton);

                _viewCache.ResearchModeToggle.Toggled += ResearchModeStateChanged;
                _viewCache.ActionButton.Clicked += OnActionButtonClicked;

                _viewCache.DesiredLevelSlider.Value = DynelManager.LocalPlayer.Level;
            }
        }

        private void ResearchModeStateChanged(object sender, bool state)
        {
            TSLeveler.ResearchMode = state;

            ActionButtonClicked?.Invoke(ButtonState.Stop);
        }

        private void OnActionButtonClicked(object sender, ButtonBase e)
        {
            ActionButtonClicked?.Invoke(_buttonState);
        }

        internal void SetActionButtonState(ButtonState state)
        {
            if (state == ButtonState.Start)
            {
                _buttonState = ButtonState.Start;
                _viewCache.ActionButton.SetLabel("Start");
            }
            else if (state == ButtonState.Stop)
            {
                _buttonState = ButtonState.Stop;
                _viewCache.ActionButton.SetLabel("Stop");
            }
        }

        public void SetDesiredLevel(int desiredLevel)
        {
            _viewCache.DesiredLevelSlider.SetValue(desiredLevel);
        }

        public void Update()
        {
            if (!_uiUpdateInterval.Elapsed)
                return;

            if (Window != null && Window.IsValid)
            {
                TSLeveler.DesiredLevel = (int)_viewCache.DesiredLevelSlider.Value;
                _viewCache.DesiredLevelSliderLabel.Text = ((int)_viewCache.DesiredLevelSlider.Value).ToString();

                if (_precheckStatusUpdateInterval.Elapsed)
                {
                    bool hasHackerTool = _itemCache.FindHackerTool(out _);
                    bool hasGraft = _itemCache.FindGrafts(out _, out _);
                    bool hasQl1SourceItem = TSLeveler.Mode == TSLevelerMode.NCU ? _itemCache.FindLockPick(out _) : _itemCache.FindImplantDisassemblyClinic(out _);
                    bool hasQl1TargetItem = TSLeveler.Mode == TSLevelerMode.NCU ? _itemCache.FindNCU(out _) : _itemCache.FindImplant(out _);

                    //bool hasSmallPool = DynelManager.LocalPlayer.GetStat(Stat.UnsavedXP) < DynelManager.LocalPlayer.GetStat(Stat.NextXP) - DynelManager.LocalPlayer.GetStat(Stat.XP);
                    bool ready = hasHackerTool && hasQl1SourceItem && hasQl1TargetItem && hasGraft/* && hasSmallPool*/;

                    _viewCache.HackerToolStatusLabel.Text = $"<font color={(hasHackerTool ? "#00FF00" : "#FF0000")}>Hacker tool (Ql 150+) in inventory {(hasHackerTool ? "&#10003;" : "&#10005;")}</font>";
                    _viewCache.GraftEquippedStatusLabel.Text = $"<font color={(hasGraft ? "#00FF00" : "#FF0000")}>Graft(s) equipped in wrists {(hasGraft ? "&#10003;" : "&#10005;")}</font>";

                    if (TSLeveler.Mode == TSLevelerMode.NCU)
                    {
                        _viewCache.ImplantDisassemblyStatusLabel.Text = $"<font color={(hasQl1SourceItem ? "#00FF00" : "#FF0000")}>Lock Pick in inventory {(hasQl1SourceItem ? "&#10003;" : "&#10005;")}</font>";
                        _viewCache.Ql1ImplantStatusLabel.Text = $"<font color={(hasQl1TargetItem ? "#00FF00" : "#FF0000")}>Ql 1 NCU equipped {(hasQl1TargetItem ? "&#10003;" : "&#10005;")}</font>";
                    }
                    else
                    {
                        _viewCache.ImplantDisassemblyStatusLabel.Text = $"<font color={(hasQl1SourceItem ? "#00FF00" : "#FF0000")}>Implant Disassembly Clinic in inventory {(hasQl1SourceItem ? "&#10003;" : "&#10005;")}</font>";
                        _viewCache.Ql1ImplantStatusLabel.Text = $"<font color={(hasQl1TargetItem ? "#00FF00" : "#FF0000")}>Ql 1 Implant (1+ clusters) equipped {(hasQl1TargetItem ? "&#10003;" : "&#10005;")}</font>";
                    }
                    //_viewCache.PoolStatusLabel.Text = $"<font color={(hasSmallPool ? "#00FF00" : "#FF0000")}>XP pool is less than remaining XP {(hasSmallPool ? "&#10003;" : "&#10005;")}</font>";

                    if (_viewCache.ActionButton.Enabled && !ready)
                        _viewCache.ActionButton.Enable(false);
                    else if (!_viewCache.ActionButton.Enabled && ready)
                        _viewCache.ActionButton.Enable(true);
                }
            }
        }

        private class ViewCache
        {
            public View Root;
            public TextView DesiredLevelSliderLabel;
            public SliderView DesiredLevelSlider;
            public TextView HackerToolStatusLabel;
            public TextView ImplantDisassemblyStatusLabel;
            public TextView Ql1ImplantStatusLabel;
            public TextView GraftEquippedStatusLabel;
            public TextView PoolStatusLabel;
            public Checkbox ResearchModeToggle;
            public Button ActionButton;
        }

        public enum ButtonState
        {
            Start,
            Stop
        }
    }
}
