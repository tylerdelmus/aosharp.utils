﻿using AOSharp.Common.Helpers;
using AOSharp.Common.Unmanaged.Imports;
using System;
using System.Runtime.InteropServices;

namespace Neko
{
    public class DynelCastSpellPatcher
    {
        private const string DynelCastSpellSig = "B8 ? ? ? ? E8 ? ? ? ? 83 EC 10 53 56 57 6A 40 8B F9 E8 ? ? ? ? 33 D2 59 3B C2 74 10 89 50 24 89 50 28 89 50 30 89 50 34 8B F0 EB 02 33 F6 8B 5D 08 33 C0 40 89 46 0C 88 46 1C 89 46 20 8B 83 ? ? ? ? 89 46 04 8B 45 0C 89 1E 83 4E 18 FF 8B 08 89 4E 24 8B 40 04 89 46 28 83 4E 08 FF 8A 45 10 88 46 2C 8A 45 14 88 46 2D 8A 45 1C 88 46 38 8A 45 20 88 46 39 8B 45 18 89 75 F0 39 10 75 12 39 50 04 75 0D";

        private const int GetAggDefOffset = 0xED;
        private const int GetAggDefBlockSize = 7;
        private const byte NOP = 0x90;

        private static bool _patched = false;

        private static IntPtr _pDynelCastSpell;
        private static IntPtr _pOrig;

        public static unsafe bool Patch()
        {
            IntPtr pDynelCastSpell = Utils.FindPattern("Gamecode.dll", DynelCastSpellSig);

            if (pDynelCastSpell == IntPtr.Zero)
                return false;

            if (!Kernel32.VirtualProtectEx(Kernel32.GetCurrentProcess(), pDynelCastSpell + GetAggDefOffset, (UIntPtr)GetAggDefBlockSize, 0x40 /* EXECUTE_READWRITE */, out uint _))
                return false;

            _pOrig = Marshal.AllocHGlobal(GetAggDefBlockSize);

            Kernel32.CopyMemory(_pOrig, pDynelCastSpell + GetAggDefOffset, GetAggDefBlockSize);

            byte[] patch = { 0xB8, 0x64, 0x00, 0x00, 0x00, NOP, NOP };

            for (int i = 0; i < GetAggDefBlockSize; i++)
                *(byte*)(pDynelCastSpell + GetAggDefOffset + i) = patch[i];

            _pDynelCastSpell = pDynelCastSpell;

            _patched = true;
            return true;
        }

        public static unsafe void Unpatch()
        {
            if (!_patched)
                return;

            Kernel32.CopyMemory(_pDynelCastSpell + GetAggDefOffset, _pOrig, GetAggDefBlockSize);
            Marshal.FreeHGlobal(_pOrig);
            Kernel32.VirtualProtectEx(Kernel32.GetCurrentProcess(), _pDynelCastSpell + GetAggDefOffset, (UIntPtr)GetAggDefBlockSize, 0x20 /* EXECUTE_READ */, out uint _);
            _patched = false;
        }
    }
}
