﻿using AOSharp.Common.GameData;
using AOSharp.Common.SharedEventArgs;
using AOSharp.Common.Unmanaged.Imports;
using AOSharp.Common.Unmanaged.Interfaces;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using static AOSharp.Core.Battlestation;

namespace Neko
{
    public class Main : AOPluginEntry
    {
        [DllImport("Utils.dll", EntryPoint = "?_Disconnect@SignalBase_c@@IBEXPAVSlotBase_c@@_N@Z", CallingConvention = CallingConvention.ThisCall)]
        public static extern void _Disconnect(IntPtr pSignal, IntPtr pSlot, bool unk);

        //TODO: Convert to settings/UI
        private bool _walkOnWater = true;
        private bool _fullAggCasting = false;
        private bool _extendedDeath = false;
        private KeyWarper _keyWarper;
        private FgridWarper _fgridWarper;

        public override void Run(string pluginDir)
        {
            Chat.RegisterCommand("fgrid", (string command, string[] param, ChatWindow chatWindow) =>
            {
                if (Inventory.Find(160978, out Item dataReceptacle))
                {
                    _fgridWarper.TryEnter(dataReceptacle.Slot);
                }
            });

            Chat.RegisterCommand("sithd", (string command, string[] param, ChatWindow chatWindow) =>
            {
                MovementController.Instance.SetMovement(MovementAction.SwitchToSit);
                MovementController.Instance.SetMovement(MovementAction.SwitchToSwim);
                MovementController.Instance.SetMovement(MovementAction.LeaveSwim);
            });

            Chat.RegisterCommand("fullaggcast", (string command, string[] param, ChatWindow chatWindow) =>
            {
                _fullAggCasting = !_fullAggCasting;

                if (_fullAggCasting)
                    DynelCastSpellPatcher.Patch();
                else
                    DynelCastSpellPatcher.Unpatch();

                Chat.WriteLine($"FullAggCasting {(_fullAggCasting ? "Enabled" : "Disabled")}");
            });

            Chat.RegisterCommand("godmode", (string command, string[] param, ChatWindow chatWindow) =>
            {
                _extendedDeath = !_extendedDeath;

                if (_extendedDeath)
                    DeathPatcher.Patch();
                else
                    DeathPatcher.Unpatch();

                Chat.WriteLine($"Extended Death {(_extendedDeath ? "Enabled" : "Disabled")}");
            });

            Chat.RegisterCommand("queueblue", (string command, string[] param, ChatWindow chatWindow) =>
            {
                DisableAutoAfk();
                DynelManager.LocalPlayer.SetStat(Stat.BattlestationSide, (int)BattlestationSide.Blue);
                Battlestation.JoinQueue(BattlestationSide.Red);
                Battlestation.JoinQueue(BattlestationSide.Blue);

            });

            Chat.RegisterCommand("queuered", (string command, string[] param, ChatWindow chatWindow) =>
            {
                DisableAutoAfk();
                DynelManager.LocalPlayer.SetStat(Stat.BattlestationSide, (int)BattlestationSide.Red);
                Battlestation.JoinQueue(BattlestationSide.Red);
                Battlestation.JoinQueue(BattlestationSide.Blue);
            });

            _keyWarper = new KeyWarper(pluginDir);
            _fgridWarper = new FgridWarper(pluginDir);

            Game.OnUpdate += OnUpdate;
            Network.N3MessageSent += OnN3MessageSent;
            Network.N3MessageReceived += OnN3MessageRecv;
            MiscClientEvents.AttemptingSpellCast += AttemptingSpellCast;

            Chat.WriteLine("Nekomimi modo");
        }

        private void OnUpdate(object s, float deltaTime)
        {
            _keyWarper.Update();

            if (_walkOnWater && DynelManager.LocalPlayer.MovementState == MovementState.Swim)
                MovementController.Instance.SetMovement(MovementAction.LeaveSwim);  
        }

        public override void Teardown()
        {
            DynelCastSpellPatcher.Unpatch();
            DeathPatcher.Unpatch();
        }

        private void DisableAutoAfk()
        {
            IntPtr pSlot = InputConfig_t.GetInstance() + 0x8;
            IntPtr pSignal = Marshal.ReadIntPtr(pSlot);

            if (pSlot != IntPtr.Zero && pSignal != IntPtr.Zero)
                _Disconnect(pSlot, pSignal, true);
        }

        private void OnN3MessageSent(object sender, N3Message n3Msg)
        {
            if (n3Msg is GenericCmdMessage genericCmdMsg && n3Msg.Identity == DynelManager.LocalPlayer.Identity && genericCmdMsg.Action == GenericCmdAction.Use)
            {
                Identity dynelId = N3EngineClientAnarchy.TemplateIDToDynelID(genericCmdMsg.Target);

                if (dynelId.Type == IdentityType.MissionKey)
                    _keyWarper.TryEnter(genericCmdMsg.Target, dynelId);
            }
        }

        private void OnN3MessageRecv(object sender, N3Message n3Msg)
        {
            if (n3Msg is FeedbackMessage feedbackMsg && feedbackMsg.Identity == DynelManager.LocalPlayer.Identity)
            {
                if (feedbackMsg.CategoryId != 0x6E)
                    return;

                if (feedbackMsg.MessageId == 0x0FCA6FF9)
                    _keyWarper.OnWrongMissionKey();
                else if (feedbackMsg.MessageId == 0x0BC6E104)
                    _keyWarper.OnKeyAccepted();
            }
        }

        private void AttemptingSpellCast(object sender, AttemptingSpellCastEventArgs e)
        {
            if (Spell.Find(e.Nano.Instance, out Spell spell))
            {
                if (_fullAggCasting)
                {
                    e.Block();

                    Network.Send(new SetStatMessage
                    {
                        Stat = Stat.AggDef,
                        Value = 100
                    });
                }

                if (_fullAggCasting && DynelManager.LocalPlayer.IsMoving)
                {
                    e.Block();

                    Network.Send(new CharDCMoveMessage
                    {
                        MoveType = MovementAction.FullStop,
                        Heading = DynelManager.LocalPlayer.Rotation,
                        Position = DynelManager.LocalPlayer.Position,
                    });

                    Coroutine.ExecuteAfter(200, () =>
                    {
                        Network.Send(new CharDCMoveMessage
                        {
                            MoveType = MovementAction.ForwardStart,
                            Heading = DynelManager.LocalPlayer.Rotation,
                            Position = DynelManager.LocalPlayer.Position,
                        });
                    });
                }

                if(e.Blocked)
                    spell.Cast();

                if (_fullAggCasting)
                {
                    Network.Send(new SetStatMessage
                    {
                        Stat = Stat.AggDef,
                        Value = DynelManager.LocalPlayer.GetStat(Stat.AggDef)
                    });
                }
            }
        }
    }
}
