﻿using AOSharp.Common.GameData;
using AOSharp.Common.Unmanaged.Interfaces;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Misc;
using AOSharp.Core.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Neko
{
    internal class FgridWarper
    {
        private Dictionary<int, List<uint>> _gridTerminals = new Dictionary<int, List<uint>>();

        internal FgridWarper(string pluginDir)
        {
            LoadGridInstances(pluginDir);
        }

        private void LoadGridInstances(string pluginDir)
        {
             _gridTerminals = JsonConvert.DeserializeObject<Dictionary<int, List<uint>>>(File.ReadAllText($"{pluginDir}\\GridTerminals.json"));
        }

        internal void TryEnter(Identity fgridSlot)
        {
            foreach(var grid in _gridTerminals.OrderByDescending(x => (PlayfieldId)x.Key == Playfield.ModelId).SelectMany(x => x.Value)) 
                Item.UseItemOnItem(fgridSlot, new Identity(IdentityType.Terminal, (int)grid));
        }
    }
}
