﻿using AOSharp.Common.GameData;
using AOSharp.Common.GameData.UI;
using AOSharp.Core;
using AOSharp.Core.Misc;
using AOSharp.Core.UI;
using AOSharp.Recast;
using org.critterai.nav;
using org.critterai.nmbuild;
using org.critterai.nmgen;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NavGen
{
    public class NavGenWindow
    {
        private Window _window;
        private string _pluginDir;
        private ViewCache _viewCache;
        private UIStateMachine _stateMachine;
        private StateData _stateData;
        private Interval _timerUpdateInterval;

        public NavGenWindow(string pluginDir)
        {
            _pluginDir = pluginDir;
            _viewCache = new ViewCache();
            _timerUpdateInterval = new Interval(1000);
            InitializeStateMachine();
        }

        public void Show()
        {
            if (_window != null && _window.IsValid)
                return;

            _window = Window.CreateFromXml($"Navmesh Generator", $"{_pluginDir}\\Views\\NavGenWindow.xml", windowStyle: WindowStyle.Default, windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);

            if (_window.FindView("Root", out _viewCache.Root))
            {
                _viewCache.Root.FindChild("WalkableHeight", out _viewCache.WalkableHeightInput);
                _viewCache.Root.FindChild("WalkableStep", out _viewCache.WalkableStepInput);
                _viewCache.Root.FindChild("WalkableRadius", out _viewCache.WalkableRadiusInput);
                _viewCache.Root.FindChild("WalkableSlope", out _viewCache.WalkableSlopeInput);
                _viewCache.Root.FindChild("XZCellSize", out _viewCache.XZCellSizeInput);
                _viewCache.Root.FindChild("YCellSize", out _viewCache.YCellSizeInput);
                _viewCache.Root.FindChild("TileSize", out _viewCache.TileSizeInput);
                _viewCache.Root.FindChild("DetailSampleDist", out _viewCache.DetailSampleDistInput);
                _viewCache.Root.FindChild("DetailMaxDeviation", out _viewCache.DetailMaxDeviationInput);
                _viewCache.Root.FindChild("MinIslandArea", out _viewCache.MinIslandAreaInput);
                _viewCache.Root.FindChild("Footer", out _viewCache.Footer);

                _stateMachine.Fire(Trigger.UILoaded);
                Populate();
            }

            _window.Show(true);
        }

        private void LoadBakeConfigFooter()
        {
            if (!_window.IsValid)
                return;

            _viewCache.Footer.DeleteAllChildren();

            View bakeFooter = View.CreateFromXml($"{_pluginDir}\\Views\\BakeFooter.xml");

            if (bakeFooter.FindChild("BakeButton", out Button bakeButton))
                bakeButton.Clicked += (s, e) => _stateMachine.Fire(Trigger.BakeButtonClicked);

            _viewCache.Footer.AddChild(bakeFooter, false);
            _viewCache.Root.FitToContents();
        }

        private void LoadBakingFooter()
        {
            if (!_window.IsValid)
                return;

            _viewCache.Footer.DeleteAllChildren();
            _viewCache.Footer.AddChild(View.CreateFromXml($"{_pluginDir}\\Views\\BakingFooter.xml"), false);
            _viewCache.Root.FitToContents();
        }

        private void LoadBakeErrorFooter()
        {
            if (!_window.IsValid)
                return;

            _viewCache.Footer.DeleteAllChildren();

            View errorFooter = View.CreateFromXml($"{_pluginDir}\\Views\\BakeErrorFooter.xml");

            if (errorFooter.FindChild("ErrorLabel", out TextView errorLabel))
            {
                errorLabel.Text = $"Failed. See chat for details.";
                Chat.WriteLine(((BakeErrorStateData)_stateData).ErrorMsg);
            }

            if (errorFooter.FindChild("ResetButton", out Button resetButton))
                resetButton.Clicked += (s, e) => _stateMachine.Fire(Trigger.Reset);

            _viewCache.Footer.AddChild(errorFooter, false);
            _viewCache.Root.FitToContents();
        }

        private void LoadBakeCompletedFooter()
        {
            if (!_window.IsValid)
                return;

            _viewCache.Footer.DeleteAllChildren();

            View completedFooter = View.CreateFromXml($"{_pluginDir}\\Views\\BakeCompletedFooter.xml");

            if (completedFooter.FindChild("ElapsedLabel", out TextView elapsedLabel))
                elapsedLabel.Text = ((BakeCompletedStateData)_stateData).Elapsed.ToString(@"mm\:ss");

            if (completedFooter.FindChild("ResetButton", out Button resetButton))
                resetButton.Clicked += (s, e) => _stateMachine.Fire(Trigger.Reset);

            if (completedFooter.FindChild("SaveButton", out Button saveButton))
            {
                saveButton.Clicked += (s, e) =>
                {
                    try
                    {
                        string filePath = $"{_pluginDir}\\Navmeshes\\{(uint)((BakeCompletedStateData)_stateData).PlayfieldId}.Navmesh";
                        Chat.WriteLine($"Saving navmesh to {filePath}");
                        ((BakeCompletedStateData)_stateData).Navmesh.Save(filePath);
                    }
                    catch (Exception ex)
                    {
                        Chat.WriteLine(ex);
                    }
                };
            }

            if (completedFooter.FindChild("ShowMeshToggle", out Checkbox showMeshToggle))
                showMeshToggle.Toggled += (s, enabled) => _stateMachine.Fire(enabled ? Trigger.DrawMeshChecked : Trigger.DrawMeshUnchecked);

            _viewCache.Footer.AddChild(completedFooter, false);
            _viewCache.Root.FitToContents();
        }

        private void ShowDrawingExtendedOptions()
        {
            if (!_window.IsValid)
                return;

            if (_viewCache.Footer.FindChild("DrawingExtendedOptionsContainer", out View drawingExtendedOptionsContainer))
            {
                _viewCache.DrawingExtendedOptionsView = View.CreateFromXml($"{_pluginDir}\\Views\\DrawingExtendedOptions.xml");

                if (_viewCache.DrawingExtendedOptionsView.FindChild("DrawingDistance", out SliderView drawDistSlider))
                    _viewCache.DrawDistSlider = drawDistSlider;

                drawingExtendedOptionsContainer.AddChild(_viewCache.DrawingExtendedOptionsView, false);
                _viewCache.Root.FitToContents();
            }
        }

        private void StartNavmeshBake()
        {
            NavGen.Config.NMGenParams.SetWalkableHeight(float.Parse(_viewCache.WalkableHeightInput.Text));
            NavGen.Config.NMGenParams.SetWalkableStep(float.Parse(_viewCache.WalkableStepInput.Text));
            NavGen.Config.NMGenParams.SetWalkableRadius(float.Parse(_viewCache.WalkableRadiusInput.Text));
            NavGen.Config.NMGenParams.WalkableSlope = float.Parse(_viewCache.WalkableSlopeInput.Text);
            NavGen.Config.NMGenParams.XZCellSize = float.Parse(_viewCache.XZCellSizeInput.Text);
            NavGen.Config.NMGenParams.YCellSize = float.Parse(_viewCache.YCellSizeInput.Text);
            NavGen.Config.NMGenParams.TileSize = int.Parse(_viewCache.TileSizeInput.Text);
            NavGen.Config.NMGenParams.DetailSampleDistance = float.Parse(_viewCache.DetailSampleDistInput.Text);
            NavGen.Config.NMGenParams.DetailMaxDeviation = float.Parse(_viewCache.DetailMaxDeviationInput.Text);
            NavGen.Config.NMGenParams.borderSize = NMGenParams.DeriveBorderSize(NavGen.Config.NMGenParams);
            NavGen.Config.NMGenParams.SetMinRegionArea(float.Parse(_viewCache.MinIslandAreaInput.Text));

            _stateData = new BakingStateData
            {
                BakingPlayfieldId = Playfield.ModelIdentity.Instance,
                BakingStartedTime = DateTime.Now
            };

            ConnectionSetCompiler connectionSetCompiler = new ConnectionSetCompiler(NavGen.OffMeshLinks.Count);

            foreach (OffMeshLink offMeshLink in NavGen.OffMeshLinks)
                connectionSetCompiler.Add(offMeshLink.Start.ToCAIVector3(), offMeshLink.End.ToCAIVector3(), 0.5f, true, NMGen.MaxArea, 1, 0);

            NavmeshGenerator.BakeAsync(NavGen.Config.NMGenParams, connectionSetCompiler.CreateConnectionSet()).ContinueWith((task) =>
            {
                if (task.IsFaulted)
                {
                    _stateData = new BakeErrorStateData
                    {
                        ErrorMsg = task.Exception.ToString()
                    };
                    _stateMachine.Fire(Trigger.BakeError);
                } 
                else
                {
                    _stateData = new BakeCompletedStateData
                    {
                        PlayfieldId = ((BakingStateData)_stateData).BakingPlayfieldId,
                        Navmesh = task.Result,
                        Elapsed = DateTime.Now - ((BakingStateData)_stateData).BakingStartedTime,
                        DrawingDist = 300
                    };
                    _stateMachine.Fire(Trigger.BakeComplete);
                }
            });
        }

        public void OnUpdate(object _, float __)
        {
            if (_window != null && _window.IsValid)
            {
                if (_timerUpdateInterval.Elapsed && 
                    _stateMachine.IsInState(State.Baking) && 
                    _viewCache.Footer.FindChild("BakingTimer", out TextView timerLabel))
                {
                    TimeSpan delta = DateTime.Now - ((BakingStateData)_stateData).BakingStartedTime;
                    timerLabel.Text = delta.ToString(@"mm\:ss");
                    _timerUpdateInterval.Reset();
                }

                DrawOffMeshLinks();
            }

            if (_stateMachine.IsInState(State.DrawingResult))
            {
                BakeCompletedStateData stateData = _stateData as BakeCompletedStateData;

                if (stateData.PlayfieldId == Playfield.ModelIdentity.Instance)
                {
                    if (_window.IsValid)
                        stateData.DrawingDist = _viewCache.DrawDistSlider.Value;

                    stateData.Navmesh.Draw(stateData.DrawingDist);
                }
            }
        }

        private void DrawOffMeshLinks()
        {
            foreach (OffMeshLink connection in NavGen.OffMeshLinks)
            {
                Debug.DrawSphere(connection.Start, 0.5f, DebuggingColor.Yellow);
                Debug.DrawSphere(connection.End, 0.5f, DebuggingColor.Yellow);
                Debug.DrawLine(connection.Start + new Vector3(0, 0.1f, 0), connection.End + new Vector3(0, 0.1f, 0), DebuggingColor.Yellow);
            }
        }

        private void Populate()
        {
            //Agent Settings
            _viewCache.WalkableHeightInput.Text = NavGen.Config.NMGenParams.WorldWalkableHeight.ToString();
            _viewCache.WalkableStepInput.Text = NavGen.Config.NMGenParams.WorldWalkableStep.ToString();
            _viewCache.WalkableRadiusInput.Text = NavGen.Config.NMGenParams.WorldWalkableRadius.ToString();
            _viewCache.WalkableSlopeInput.Text = NavGen.Config.NMGenParams.WalkableSlope.ToString();

            //Resolution and Tile Settings
            _viewCache.XZCellSizeInput.Text = NavGen.Config.NMGenParams.XZCellSize.ToString();
            _viewCache.YCellSizeInput.Text = NavGen.Config.NMGenParams.YCellSize.ToString();
            _viewCache.TileSizeInput.Text = NavGen.Config.NMGenParams.TileSize.ToString();

            //Miscellaneous Settings
            _viewCache.DetailSampleDistInput.Text = NavGen.Config.NMGenParams.DetailSampleDistance.ToString();
            _viewCache.DetailMaxDeviationInput.Text = NavGen.Config.NMGenParams.DetailMaxDeviation.ToString();
            _viewCache.MinIslandAreaInput.Text = NavGen.Config.NMGenParams.MinRegionArea.ToString();
        }

        public Navmesh GetNavmesh()
        {
            if(_stateData is BakeCompletedStateData stateData)
                return stateData.Navmesh;

            return null;
        }

        private void InitializeStateMachine()
        {
            _stateMachine = new UIStateMachine();

            _stateMachine.Configure(State.Default)
                .Permit(Trigger.UILoaded, State.AwaitingBake);

            _stateMachine.Configure(State.AwaitingBake)
                .OnEntry(() =>
                {
                    LoadBakeConfigFooter();
                })
                .Permit(Trigger.BakeButtonClicked, State.Baking)
                .PermitReentry(Trigger.UILoaded);

            _stateMachine.Configure(State.Baking)
                .OnEntryFrom(Trigger.BakeButtonClicked, () => StartNavmeshBake())
                .OnEntry(() => LoadBakingFooter())
                .Permit(Trigger.BakeError, State.BakeFailed)
                .Permit(Trigger.BakeComplete, State.BakeCompleted)
                .PermitReentry(Trigger.UILoaded);

            _stateMachine.Configure(State.BakeFailed)
                .OnEntry(() => LoadBakeErrorFooter())
                .Permit(Trigger.Reset, State.AwaitingBake)
                .PermitReentry(Trigger.UILoaded);

            _stateMachine.Configure(State.BakeCompleted)
                .OnEntry(() => LoadBakeCompletedFooter())
                .Permit(Trigger.Reset, State.AwaitingBake)
                .Permit(Trigger.DrawMeshChecked, State.DrawingResult)
                .PermitReentry(Trigger.UILoaded)
                .PermitReentry(Trigger.DrawMeshUnchecked);

            _stateMachine.Configure(State.DrawingResult)
                .SubstateOf(State.BakeCompleted)
                .OnEntry(() => ShowDrawingExtendedOptions())
                .Permit(Trigger.Reset, State.AwaitingBake)
                .PermitReentry(Trigger.UILoaded);
        }

        private class ViewCache
        {
            public View Root;
            public View Footer;
            public View DrawingExtendedOptionsView;
            public SliderView DrawDistSlider;
            public TextInputView WalkableHeightInput;
            public TextInputView WalkableStepInput;
            public TextInputView WalkableRadiusInput;
            public TextInputView WalkableSlopeInput;
            public TextInputView XZCellSizeInput;
            public TextInputView YCellSizeInput;
            public TextInputView TileSizeInput;
            public TextInputView DetailSampleDistInput;
            public TextInputView DetailMaxDeviationInput;
            public TextInputView MinIslandAreaInput;
        }

        private abstract class StateData {}

        private class BakingStateData : StateData
        {
            public int BakingPlayfieldId;
            public DateTime BakingStartedTime;
            //public float Progress;
        }

        private class BakeErrorStateData : StateData
        {
            public string ErrorMsg;
        }

        private class BakeCompletedStateData : StateData
        {
            public int PlayfieldId;
            public Navmesh Navmesh;
            public TimeSpan Elapsed;
            public float DrawingDist;
        }
    }
}
