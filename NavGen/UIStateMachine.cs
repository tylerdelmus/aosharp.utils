﻿using AOSharp.Core.UI;
using Stateless;

namespace NavGen
{
    internal enum Trigger
    {
        UILoaded,
        BakeButtonClicked,
        BakeError,
        BakeComplete,
        DrawMeshChecked,
        DrawMeshUnchecked,
        Zoned,
        Reset
    }

    internal enum State
    {
        Default,
        AwaitingBake,
        Baking,
        BakeFailed,
        BakeCompleted,
        DrawingResult,
        InInvalidZone
    }

    internal class UIStateMachine : StateMachine<State, Trigger>
    {
        public UIStateMachine() : base(State.Default)
        {
            //OnTransitioned(OnTransitionAction);
        }

        private void OnTransitionAction(Transition obj)
        {
            Chat.WriteLine($"State transition from {obj.Source} to {obj.Destination} triggered by {obj.Trigger}. Re-entry is {obj.IsReentry}");
        }
    }
}
