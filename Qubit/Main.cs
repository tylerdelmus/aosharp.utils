﻿using AOSharp.Common.GameData.UI;
using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Core.Inventory;
using AOSharp.Common.Unmanaged.Imports;
using System.Runtime.InteropServices;
using SmokeLounge.AOtomation.Messaging.GameData;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using AOSharp.Common.Helpers;

namespace Qubit
{
    public class Main : AOPluginEntry
    {
        private UsablesWindow _window;

        public override void Run()
        {
            try
            {
                _window = new UsablesWindow($"{PluginDirectory}\\Views\\UsablesWindow.xml");
                _window.Open();

                Game.OnUpdate += Update;
                Game.TeleportStarted += OnBeginTeleport;
            }
            catch (Exception ex)
            {
                Chat.WriteLine(ex.Message);
            }
        }

        private void OnBeginTeleport(object sender, EventArgs e)
        {
            _window.OnTeleport();
        }

        private void Update(object sender, float e)
        {
            _window.Update();
        }
    }
}
