﻿using AOSharp.Common.GameData;
using AOSharp.Common.Unmanaged.Imports;
using AOSharp.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Qubit
{
    public class Utils
    {
        public static unsafe List<Dynel> GetDynels()
        {
            IntPtr pDynelMap = N3Dynel_t.GetDynelMap();

            IntPtr pRoot = *(IntPtr*)(pDynelMap + 4);

            if (pRoot == IntPtr.Zero)
                return new List<Dynel>();

            IntPtr pEntryElement = *(IntPtr*)(pRoot + 4);

            if (pEntryElement == IntPtr.Zero)
                return new List<Dynel>();

            List<IntPtr> dynels = new List<IntPtr>();
            GetAllEntries(pEntryElement, dynels);
            dynels.Remove(pRoot);

            return dynels.Select(p => new Dynel(*(IntPtr*)(p + 0x14))).ToList();
        }

        private static unsafe void GetAllEntries(IntPtr pOrigin, List<IntPtr> dynels)
        {
            List<IntPtr> entries = new List<IntPtr>();

            dynels.Add(pOrigin);

            IntPtr left = *(IntPtr*)(pOrigin + 0x0);
            IntPtr right = *(IntPtr*)(pOrigin + 0x8);

            if (left == right)
                return;

            if (!dynels.Contains(left))
                GetAllEntries(left, dynels);

            if (!dynels.Contains(right))
                GetAllEntries(right, dynels);
        }
    }
}
